<?php

class BookingsModel
{
    public function create ($data)
    {
        $database = new Database();
        $sql = "INSERT INTO `bookings` (`res_date`, `res_time`, `guests`, `name`, `phone`) VALUES (?, ?, ?, ?, ?)";

        $data_to_insert = [
            $data['res_date'],
            $data['res_time'],
            $data['guests'],
            $data['name'],
            $data['phone']
        ];

        $database->executeSql($sql, $data_to_insert);

        $sql = "INSERT INTO `users` (`first_name`, `phone`, `role`) VALUES (?, ?, ?)";
        $role = 'guest';

        $data_to_insert = [
            $data['name'],
            $data['phone'],
            $role
        ];

       $database->executeSql($sql, $data_to_insert);
       return true;
    }

    private function validateFormData($data){
        $valid = false;

        // atliekamas formos duomenu tikrinimas
        // ...

        return $valid;
    }
}