<?php

class MenuModel {

    public function listAll()
    {
        // Susikuriame duombazes objekta
        $database = new Database();

        // Paruosiame uzklausa
        $sql = 'SELECT * FROM meals';

        // Siunciame uzklausa ir graziname gauta reiksme
        return $database->query($sql);
    }

    public function listById($productID)
    {
        $database = new Database();
        $sql = 'SELECT * FROM meals WHERE id = ' . $productID;
        return $database->queryOne($sql);
    }


}