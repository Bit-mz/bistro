<?php

class PaymentModel
{
    public function create ($data)
    {
        $database = new Database();
        $sql = "INSERT INTO `order` (`user_id`, `amount_total`, `tax_rate`, `amount_tax`) VALUES (?, ?, ?, ?)";

        $user_id = $_SESSION['user_id'];
        $total = $_SESSION['totalSum'];
        $tax_amount = $total * TAX_RATE;

        $data_to_insert =
            [
                $user_id,
                $total,
                TAX_RATE,
                $tax_amount,
            ];

        $new_order_id = $database->executeSql($sql, $data_to_insert);

        foreach ($_SESSION['cart'] as $item) {

            $sql = "INSERT INTO `orderline` (`qty_ordered`, `meal_id`, `order_id`, `price_each`) VALUES (?, ?, ?, ?)";

            $qty = $item['qty_ordered'];
            $meal_id = $item['id'];
            $order_id = $new_order_id;
            $price_each = $item['SalePrice'];

            $data_to_insert =
                [
                    $qty,
                    $meal_id,
                    $order_id,
                    $price_each,
                ];

            $database->executeSql($sql, $data_to_insert);
        }

        return true;
    }

}