<?php

class RegistrationModel
{
    public function create ($data)
    {
        $database = new Database();
        $sql = "INSERT INTO `users` (`first_name`, `last_name`, `email`, `password`, `role`) VALUES (?, ?, ?, ?, ?)";
        $password = password_hash($data['password'], PASSWORD_DEFAULT);
        $role = 'user';

            $data_to_insert = [
            $data['first_name'],
            $data['last_name'],
            $data['email'],
            $password,
            $role
        ];

        $id = $database->executeSql($sql, $data_to_insert);

        return $id;
    }

    public function loginUser($user_id){
        $database = new Database();

        $sql = "SELECT * FROM users WHERE id = ?";

        $user = $database->queryOne($sql, [$user_id]);

        if($user){

            $_SESSION['logged'] = true;
            $_SESSION['name'] = $user['first_name'];
            $_SESSION['role'] = $user['role'];

            return true;
        }else{
            return false;
        }
    }
}