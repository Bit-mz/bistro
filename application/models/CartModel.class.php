<?php

class CartModel
{
    public function create ($data)
    {
        $database = new Database();
        $sql = "INSERT INTO `orderline` (`meal_id`, `qty_ordered`, `price_each`) VALUES (?, ?, ?)";

        $data_to_insert =
            [
                $data['meal_id'],
                $data['qty_ordered'],
                $data['price_each'],
            ];

        $database->executeSql($sql, $data_to_insert);

        return true;
    }

}