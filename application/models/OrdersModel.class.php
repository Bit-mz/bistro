<?php

class OrdersModel {

    public function listAll()
    {
        // Susikuriame duombazes objekta
        $database = new Database();

        // Paruosiame uzklausa
        $sql = 'SELECT * FROM meals';

        // Siunciame uzklausa ir graziname gauta reiksme
        return $database->query($sql);
    }
}