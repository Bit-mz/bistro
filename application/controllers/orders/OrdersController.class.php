<?php


class OrdersController
{

    public function httpGetMethod(Http $http, array $queryFields)
    {

        // Sukuriame modelio objekta
        $ordersModel = new OrdersModel();

        /*
         * Kreipiames i
        modelio klases fuenkcija ir
        priskiriame reiksmei
        */
        $orders = $ordersModel->listAll();

        $totalValue = $this->getTotalValue($orders);


        // Perduodame reiksmes i vei
        return [
            'orders' => $orders,
            'totalValue' => $totalValue
        ];
    }

    public function httpPostMethod(Http $http, array $queryFields)
    {

    }

    public function getTotalValue($orders)
    {
        $totalValue = 0;
        foreach ($orders as $order) {
            $totalValue += $order['SalePrice'] * $order['QuantityInStock'];
        }

        return $totalValue;
    }
}