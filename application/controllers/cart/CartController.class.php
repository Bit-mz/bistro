<?php

class CartController
{
    public function httpGetMethod(Http $http, array $queryFields)
    {

        return [
            'cart' => $_SESSION['cart']
        ];
    }

    public function httpPostMethod(Http $http, array $queryFields)
    {
        $cart = new CartModel();

        if($queryFields['type'] == 'remove'){
            $id = $queryFields['item_id'];

            foreach($_SESSION['cart'] as $key => $cart_item){
                if($cart_item['id'] == $id){
                    unset($_SESSION['cart'][$key]);
                    break;
                }
            }

            $_SESSION['totalSum'] = sumArrayByField($_SESSION['cart'], 'item_total');
        }

        if($queryFields['type'] == 'checkout')
        {
            if (isset($_SESSION['user']))
            {
                $http->redirectTo("/payment");
            } else
            {
                $_SESSION['redirect_url'] = "/payment";
                $http->redirectTo("/login?status=1");
            }
        }

        return [
            'cart' => $_SESSION['cart']
        ];



    }

}