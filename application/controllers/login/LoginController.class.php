<?php

class LoginController
{
    public function httpGetMethod(Http $http, array $queryFields)
    {

    }

    public function httpPostMethod(Http $http, array $queryFields)
    {
        $login = new LoginModel();

        $user_authorized = $login->checkCreds($queryFields['email'], $queryFields['password']);

        if($user_authorized){
            if(isset($_SESSION['redirect_url'])){
                $redirect_url = $_SESSION['redirect_url'];
                unset($_SESSION['redirect_url']);
                $http->redirectTo($redirect_url);
            }else {
                $http->redirectTo("/");
            }
        } else{
            $http->redirectTo("/login?status=0");
        }

    }
}