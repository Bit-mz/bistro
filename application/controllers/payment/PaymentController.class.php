<?php

class PaymentController
{
    public function httpGetMethod(Http $http, array $queryFields)
    {

        return [
            'cart' => $_SESSION['cart']
        ];
    }

    public function httpPostMethod(Http $http, array $queryFields)
    {
        $paymentModel = new PaymentModel();

        $paymentData = $paymentModel->create($queryFields);

        unset ($_SESSION['cart']);
        $_SESSION['totalSum'] = 0;

        $http->redirectTo("/?status=1");
    }

}