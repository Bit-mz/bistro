<?php

class BookingsController
{
    public function httpGetMethod(Http $http, array $queryFields)
    {

    }

    public function httpPostMethod(Http $http, array $queryFields)
    {
        $BookingsModel = new BookingsModel();

        $result = $BookingsModel->create($queryFields);

        if($result){
            $http->redirectTo("/?status=2");
        }else{
            $http->redirectTo("/?status=99");
        }

    }
}