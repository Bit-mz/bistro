<?php


class MenuController
{

    public function httpGetMethod(Http $http, array $queryFields)
    {

        $menuModel = new MenuModel();

        $menu = $menuModel->listAll();

        $totalSum = 0;
        for($i = 0; $i < count($_SESSION['cart']); $i++) {
            $totalSum += $_SESSION['cart'][$i]["SalePrice"] * $_SESSION['cart'][$i]["qty_ordered"];
        }

        $_SESSION['totalSum'] = $totalSum;


        return ['menu' => $menu,
        'totalSum' => $totalSum
        ];

    }

    public function httpPostMethod(Http $http, array $queryFields)
    {
        $menuModel = new MenuModel();

        $product = $menuModel->listById($queryFields['item_id']);

        $exist = false;

        if(count($_SESSION['cart']) > 0) {
            for ($i = 0; $i < count($_SESSION['cart']); $i++) {
                if($_SESSION['cart'][$i]['id'] == $queryFields['item_id']){
                    $exist = true;

                    if(isset($_SESSION['cart'][$i]['qty_ordered'])){
                        $_SESSION['cart'][$i]['qty_ordered'] += $queryFields['qty_ordered'];
                    }else{
                        $_SESSION['cart'][$i]['qty_ordered'] = $queryFields['qty_ordered'];
                    }

                    if(isset($_SESSION['cart'][$i]['item_total'])) {
                        $_SESSION['cart'][$i]['item_total'] += $queryFields['qty_ordered'] * $product['SalePrice'];
                    }else{
                        $_SESSION['cart'][$i]['item_total'] = $queryFields['qty_ordered'] * $product['SalePrice'];
                    }
                }
            }
        }


        if(!$exist) {
            $product['qty_ordered'] = $queryFields['qty_ordered'];
            $product['item_total'] = $queryFields['qty_ordered'] * $product['SalePrice'];
            array_push($_SESSION['cart'], $product);
        }



        $http->redirectTo("/menu");
    }
}