'use strict';

/////////////////////////////////////////////////////////////////////////////////////////
// FONCTIONS                                                                           //
/////////////////////////////////////////////////////////////////////////////////////////




/////////////////////////////////////////////////////////////////////////////////////////
// CODE PRINCIPAL                                                                      //
/////////////////////////////////////////////////////////////////////////////////////////

$(document).ready(function(){

    $('select[name="hours"], select[name="minutes"]').change(function(){
        var hours = $('select[name="hours"]').val();
        var minutes = $('select[name="minutes"]').val();
        var time = hours + ':' + minutes;

        $('input[name="res_time"]').val(time);
        console.log(time);
    })

    $('.add-to-cart').click(function(){

        var product_id = $(this).data('product-id');

        alert(product_id);

    })

})