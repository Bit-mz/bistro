2016-08-29 Gironi's Bistro project

USERS
User logins for testing (username / password):
	1. Regular user (jz@generama.lt / jz)
	2. Admin (mz@generama.lt / mz)

DATABASE

['dsn']      = 'mysql:host=localhost;dbname=restoranas';
$config['password'] = '';
$config['user']     = 'root';

DB dump file is in root directory, named "restoranas.sql".