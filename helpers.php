<?php
session_start();

const TAX_RATE = 0.21;

if(!isset($_SESSION['totalSum'])) {
    $_SESSION['totalSum'] = 0;

}

if(!isset($_SESSION['cart'])) {
    $_SESSION['cart'] = [];

}

if(isset($_GET['logout'])){
    session_unset();
    session_destroy();
    $http = new Http();
    $http->redirectTo('/');
}

function dd($data){
    echo "<pre>";
    var_dump($data);
    echo "/<pre>";
    exit();
}

function sumArrayByField ($array, $field)
{
    $sum = 0;

    foreach ($array as $item)
    {
        if (isset($item[$field]))
        {
            $sum += $item[$field];
        }
    }

    return $sum;
}