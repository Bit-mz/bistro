-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2016 m. Rgp 30 d. 08:30
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `restoranas`
--

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `bookings`
--

CREATE TABLE `bookings` (
  `id` int(11) NOT NULL,
  `res_date` date NOT NULL,
  `res_time` time NOT NULL,
  `guests` tinyint(4) NOT NULL,
  `name` tinytext NOT NULL,
  `phone` varchar(15) NOT NULL,
  `table_no` tinyint(4) NOT NULL,
  `user_id` smallint(6) NOT NULL,
  `creation_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sukurta duomenų kopija lentelei `bookings`
--

INSERT INTO `bookings` (`id`, `res_date`, `res_time`, `guests`, `name`, `phone`, `table_no`, `user_id`, `creation_timestamp`) VALUES
(6, '2016-08-19', '19:00:00', 3, 'Mindaugas', '868742106', 0, 0, '2016-08-18 10:19:05'),
(7, '2016-08-20', '20:00:00', 6, 'Mindaugas', '868742821', 0, 0, '2016-08-18 10:54:58'),
(8, '2016-08-21', '00:00:00', 4, 'Izabella', '868741205', 0, 0, '2016-08-19 12:44:32'),
(9, '2016-08-21', '00:00:00', 4, 'Izabella', '868741205', 0, 0, '2016-08-19 12:48:13'),
(10, '2016-08-21', '00:00:00', 4, 'Izabella', '868741205', 0, 0, '2016-08-19 12:48:19'),
(11, '2016-08-22', '15:00:00', 4, 'Rita', '868745214', 0, 0, '2016-08-19 12:49:05'),
(12, '2016-08-22', '15:00:00', 4, 'Rita', '868745214', 0, 0, '2016-08-19 12:51:12'),
(13, '0000-00-00', '00:00:00', 0, '', '', 0, 0, '2016-08-22 07:39:23'),
(14, '0000-00-00', '00:00:00', 0, '', '', 0, 0, '2016-08-26 12:35:40'),
(15, '2016-08-25', '00:00:00', 2, '', '', 0, 0, '2016-08-26 12:43:04'),
(16, '0000-00-00', '00:00:00', 0, '', '', 0, 0, '2016-08-26 12:50:46'),
(17, '2016-08-02', '22:30:00', 2, 'John', 'Smith', 0, 0, '2016-08-26 13:10:48');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `meals`
--

CREATE TABLE `meals` (
  `id` int(11) NOT NULL,
  `Name` varchar(30) NOT NULL,
  `Photo` varchar(30) NOT NULL,
  `Description` varchar(250) NOT NULL,
  `QuantityInStock` tinyint(4) NOT NULL,
  `BuyPrice` double NOT NULL,
  `SalePrice` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sukurta duomenų kopija lentelei `meals`
--

INSERT INTO `meals` (`id`, `Name`, `Photo`, `Description`, `QuantityInStock`, `BuyPrice`, `SalePrice`) VALUES
(1, 'Coca-Cola', 'coca.jpg', 'Mmmm, le Coca-Cola avec 10 morceaux de sucres et tout plein de caféine !', 72, 0.6, 3),
(2, 'Bagel Thon', 'bagel_thon.jpg', 'Notre bagel est constitué d''un pain moelleux avec des grains de sésame et du thon albacore, accompagné de feuilles de salade fraîche du jour  et d''une sauce renversante :-)', 18, 2.75, 5.5),
(3, 'Bacon Cheeseburger', 'bacon_cheeseburger.jpg', 'Ce délicieux cheeseburger contient un steak haché viande française de 150g ainsi que d''un buns grillé juste comme il faut, le tout accompagné de frites fraîches maison !', 14, 6, 12.5),
(4, 'Carrot Cake', 'carrot_cake.jpg', 'Le carrot cake maison ravira les plus gourmands et les puristes : tous les ingrédients sont naturels !\r\nÀ consommer sans modération', 9, 3, 6.75),
(5, 'Donut Chocolat', 'chocolate_donut.jpg', 'Les donuts sont fabriqués le matin même et sont recouvert d''une délicieuse sauce au chocolat !', 16, 3, 6.2),
(6, 'Dr. Pepper', 'drpepper.jpg', 'Son goût sucré avec de l''amande vous ravira !', 53, 0.5, 2.9),
(7, 'Milkshake', 'milkshake.jpg', 'Notre milkshake bien crémeux contient des morceaux d''Oréos et est accompagné de crème chantilly et de smarties en guise de topping. Il éblouira vos papilles !', 12, 2, 5.35);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount_total` double NOT NULL,
  `tax_rate` float NOT NULL,
  `amount_tax` double NOT NULL,
  `creation_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `complete_timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sukurta duomenų kopija lentelei `order`
--

INSERT INTO `order` (`id`, `user_id`, `amount_total`, `tax_rate`, `amount_tax`, `creation_timestamp`, `complete_timestamp`) VALUES
(14, 13, 8.5, 0.21, 1.785, '2016-08-26 14:20:28', '0000-00-00 00:00:00'),
(17, 13, 15.5, 0.21, 3.255, '2016-08-26 14:25:29', '0000-00-00 00:00:00'),
(18, 13, 9.75, 0.21, 2.0475, '2016-08-26 14:30:23', '0000-00-00 00:00:00'),
(19, 13, 3, 0.21, 0.63, '2016-08-26 14:45:11', '0000-00-00 00:00:00'),
(20, 13, 3, 0.21, 0.63, '2016-08-26 15:17:12', '0000-00-00 00:00:00'),
(21, 13, 0, 0.21, 0, '2016-08-26 15:17:41', '0000-00-00 00:00:00'),
(22, 13, 3, 0.21, 0.63, '2016-08-26 15:23:44', '0000-00-00 00:00:00'),
(23, 13, 3, 0.21, 0.63, '2016-08-26 15:30:18', '0000-00-00 00:00:00'),
(24, 13, 3, 0.21, 0.63, '2016-08-26 17:06:46', '0000-00-00 00:00:00'),
(25, 13, 3, 0.21, 0.63, '2016-08-29 09:15:09', '0000-00-00 00:00:00'),
(26, 13, 8.5, 0.21, 1.785, '2016-08-30 09:28:55', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `orderline`
--

CREATE TABLE `orderline` (
  `id` int(11) NOT NULL,
  `qty_ordered` int(4) NOT NULL,
  `meal_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `price_each` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sukurta duomenų kopija lentelei `orderline`
--

INSERT INTO `orderline` (`id`, `qty_ordered`, `meal_id`, `order_id`, `price_each`) VALUES
(19, 1, 1, 14, 3),
(20, 1, 2, 14, 5.5),
(21, 1, 3, 17, 12.5),
(22, 1, 1, 17, 3),
(23, 1, 1, 18, 3),
(24, 1, 4, 18, 6.75),
(25, 1, 1, 19, 3),
(26, 1, 1, 20, 3),
(27, 1, 1, 22, 3),
(28, 1, 1, 23, 3),
(29, 1, 1, 24, 3),
(30, 1, 1, 25, 3),
(31, 1, 1, 26, 3),
(32, 1, 2, 26, 5.5);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(25) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(75) NOT NULL,
  `dob` date NOT NULL,
  `address` varchar(250) NOT NULL,
  `city` varchar(40) NOT NULL,
  `zipcode` varchar(8) NOT NULL,
  `country` varchar(20) NOT NULL,
  `phone` char(15) NOT NULL,
  `role` varchar(10) NOT NULL,
  `creation_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login_timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sukurta duomenų kopija lentelei `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `dob`, `address`, `city`, `zipcode`, `country`, `phone`, `role`, `creation_timestamp`, `last_login_timestamp`) VALUES
(12, 'Mindaugas', 'Zironas', 'mz@generama.lt', '$2y$10$uX/v6NWM0/esOekThA.sMuxVHgVtShTx4PI8CpVLCSvnagUSGW7Om', '0000-00-00', '', '', '', '', '', 'admin', '2016-08-19 14:42:27', '0000-00-00 00:00:00'),
(13, 'Jonas', 'Zironas', 'jz@generama.lt', '$2y$10$E13OoCrvQLFG5rlrDABGC.eK4t9D1S1kYuPw10sll70oFiFH2vmeC', '0000-00-00', '', '', '', '', '', 'user', '2016-08-19 14:42:42', '0000-00-00 00:00:00'),
(15, 'Izabella', '', '', '', '0000-00-00', '', '', '', '', '868741205', 'guest', '2016-08-19 15:48:13', '0000-00-00 00:00:00'),
(16, 'Izabella', '', '', '', '0000-00-00', '', '', '', '', '868741205', 'guest', '2016-08-19 15:48:19', '0000-00-00 00:00:00'),
(17, 'Rita', '', '', '', '0000-00-00', '', '', '', '', '868745214', 'guest', '2016-08-19 15:49:05', '0000-00-00 00:00:00'),
(18, 'Rita', '', '', '', '0000-00-00', '', '', '', '', '868745214', 'guest', '2016-08-19 15:51:12', '0000-00-00 00:00:00'),
(19, '', '', '', '', '0000-00-00', '', '', '', '', '', 'guest', '2016-08-22 10:39:23', '0000-00-00 00:00:00'),
(20, '', '', '', '', '0000-00-00', '', '', '', '', '', 'guest', '2016-08-26 15:35:40', '0000-00-00 00:00:00'),
(21, '', '', '', '', '0000-00-00', '', '', '', '', '', 'guest', '2016-08-26 15:43:04', '0000-00-00 00:00:00'),
(22, '', '', '', '$2y$10$Y6P4UXgNTg8.Q8d8M09/zO8/OjdeNxziOBv//xiryIJLs/VcyKyMq', '0000-00-00', '', '', '', '', '', 'user', '2016-08-26 15:44:09', '0000-00-00 00:00:00'),
(23, '', '', '', '', '0000-00-00', '', '', '', '', '', 'guest', '2016-08-26 15:50:46', '0000-00-00 00:00:00'),
(24, 'John', '', '', '', '0000-00-00', '', '', '', '', 'Smith', 'guest', '2016-08-26 16:10:48', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `meals`
--
ALTER TABLE `meals`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderline`
--
ALTER TABLE `orderline`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `meals`
--
ALTER TABLE `meals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `orderline`
--
ALTER TABLE `orderline`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
